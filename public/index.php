<?php
/** le routeur **/

$uri = $_SERVER['REQUEST_URI']; // on recupere l'url 

if($result = match($uri, "/")) // result prend 1 ou 0 en fct de la fcct match
{
    header("location:../articles"); //si dans l'url ya rien on redirige 
    die;
}

####################
### Crud Article ###
####################

/** on require note controlleur si ca la fonction a bien match**/

if($result = match($uri, "/article/create")){
    require("../Controller/article/createArticle.php");
    die;
}

if($result = match($uri, "/article/delete/:id")){
    require("../Controller/article/deleteArticle.php");
    die;
}

if($result = match($uri, "/article/:id")){
    require("../Controller/article/displayArticle.php");
    die;
}

if($result = match($uri, "/articles")){
    require("../Controller/article/displayArticles.php");
    die;
}

if($result = match($uri, "/article/update/:id")){
    require("../Controller/article/updateArticle.php");
    die;
}

#################
### Crud User ###
#################
if($result = match($uri, "/user/create")){
    require("../Controller/user/createUser.php");
    die;
}

if($result = match($uri, "/user/delete/:id")){
    require("../Controller/user/deleteUser.php");
    die;
}

if($result = match($uri, "/user/:id")){
    require("../Controller/user/displayUser.php");
    die;
}

if($result = match($uri, "/users")){
    require("../Controller/user/displayUsers.php");
    die;
}

if($result = match($uri, "/user/update/:id")){
    require("../Controller/user/updateUser.php");
    die;
}

echo "No routes are matching with " . $uri;

/*notre fct matche verfie la correspondance entre l'url et notre routes retourne faux si ca match pas et vrai si ca match*/

function match($url, $route){
    $path = preg_replace('#:([\w]+)#', '([^/]+)', $route);
    $regex = "#^$path$#i";
    if(!preg_match($regex, $url, $matches)){
        return false;
    }
    return true;
}