<?php
$errors = new ArrayObject(); // creation d'un tableau pour les erreurs
$title="display User"; // titre pour template 
$userId = getUserIdFromURI();  // on recupere l'userid depuis la fct getUserIdFromURI voir commmentaire

require('../Model/userRepository.php'); // on require le model pour choper nos fct

$response = getUser($userId); //  on appel la fct getUser on recupere toute les information de notre 
//user on met ca dans response  


if(!userExist($response)){ // si la fct userExist renvoie vrai (si l'utilassauteur n'existe pas) .. 
    $errors->append('Sorry there is no user id ' . $userId); // prend une erreur dans le tableau 
    displayErrors($errors); // on appel la fct displayErrors qui affiche l'erreur
    die;// on tue le script
}

ob_start(); // debut du contenue du template
displayErrors($errors); // on appel la fct displayErrors qui affiche l'erreur

require('../view/user/displayUserView.php');  // on require notre vue 

$content=ob_get_clean();  // fin du contenue on met tout ca dans la variable content

require('../view/templateView.php'); // le template on va utiliser title et content pour e remplir

$response->closeCursor(); //on ferme le curseur car on a getUser


function getUserIdFromURI() // fct qui permet de recuperer le dernier element de l'url (l'id du coup)
{
    $monUrl = $_SERVER['REQUEST_URI']; //on recuper l'url entier
    $monUrl = explode("/", $monUrl) ; // on separe chaque element ou ya un slash
    $authorId = end($monUrl) ; // on prend le dernier element en l'occurence notre id 

    return $authorId; // on le retourne
}

/* fct qui parcoure l'ensemble du tableau d'erreur en les affichant  prend le tableau en paremetre*/
function displayErrors($errors)
{
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}
/*si le user existe on renvoie vrai sinon faux */
function userExist($response)
{
    if($response->rowCount() > 0 ) // compte le nb de ligne si sup a 0 ca veux dire ya un id qui match
    {
        return true; // renvoie vrai si c bon 
    }
    return false;// sinon renvoie faux 

}