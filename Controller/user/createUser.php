<?php
session_start();
$title="create User"; // titre pour le template
$errors = new ArrayObject(); // creation d'un tableau pour les erreurs

if(isFormValid($errors)){ // si le formulaire est valide  ... 

    $email = $_POST['email']; // on recupere l'email
    $password = $_POST['password']; // on recupere le mot de passe 1
    $confirm_password = $_POST['confirm_password']; // on recupere le mot de passe 2

    require("../Model/userRepository.php"); // on require le model pour choper nos fct
    $bdd = dbConnect(); // on appel la fct qui est dans le model qui permet la connection 
    $response = createUser($bdd, $email, $password); // on appel la fct  create voir com
    //qui est dans le model et on lui passe en parametre la bdd l'email et le mot de passe 
    $lastInsertedId = $bdd->lastInsertId(); // la creation faite on recupere l'id a l'endroit ou on //vient d'insere nos information   

//    var_dump($lastInsertedId);

    if($lastInsertedId>0){ // si >0 cad si ya bien eu une insertion valide
        header("location:../user/" . $lastInsertedId);die; // on redirige vers notre display user creer
    }
    $errors->append("An error occurred, please contact your administrator system!");// sinon notre tableau prend une erreur
}

ob_start(); // debut du contenue du template
displayErrors($errors); // appel de la fct qui affiche les erreurs

require("../view/user/createUserView.php"); // on require notre vue 

$content=ob_get_clean(); // fin du contenue on met tout ca dans la variable content

require("../view/templateView.php"); // le template on va utiliser title et content pour e remplir


function isFormValid(ArrayObject $errors){  //fct qui verifie si le form est valide
    if(!variablesAreSet()){ // si la fct variablesAreSet renvoie 0 ...
        $errors->append('First time in the page'); // le tableau prend une erreur
        return false;// on retoune faux pour ne pas rentrer dans le if isFormValid($errors) 
    }

    $email = $_POST['email'];  // on recupere l'email
    $password = $_POST['password']; // on recupere le mot de passe 1
    $confirm_password = $_POST['confirm_password']; // on recupere le mot de passe 2

    /* si la fct fieldsArefilled renvoie 0 ... */
    if(!fieldsArefilled($email, $password, $confirm_password)){ 

        $errors->append('All the fields must be filled'); // on prend une erreurs
        return false; // on retoune faux pour ne pas rentrer dans le if isFormValid($errors)  
    }

    if($password != $confirm_password){ // si mot de passe1  different de mot de passe2 ... 
        $errors->append('confirmation password error'); // on prend une erreurs
        return false; // on retoune faux pour ne pas rentrer dans le if isFormValid($errors) 
    }

    return true; // on a verifier tout les cas d'erreur on on renvoyer vrai si du coup le form est valide
}

function variablesAreSet(){ // fct qui verfie si les variable sont presente dans la pages
    //Get data. If the user come directly in this page. He is redirected
    /* on verifie l'email le mdp  et le mdp2*/
    if(isset($_POST['email']) AND isset($_POST['password']) AND isset($_POST['confirm_password'])){
        return true; // si c'est good on renvoie vrai
    }

    return false; // si c'est pas bon on renvoie fauxx
}
/*// fct qui verfie si les variable sont bien remplie*/
function fieldsArefilled($email, $password, $confirm_password)
{ 
    //If the user does not fill all the fields a error message is set and he is redirected
    /* si elle sont vide on renvoye faux si elle sont remplie on renvoye vrai*/
    if(empty($email) OR empty($password) OR empty($confirm_password)){
        return false;
    }
    return true;
}
/* fct qui parcoure l'ensemble du tableau d'erreur en les affichant */
function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}




