<?php
/*fct qui chope l'id et l'email de tout les user*/
function getUsers(){
    $bdd = dbConnect();// connection 
    /*preparation de la requete */
    $response = $bdd->prepare ('SELECT u.id, u.email FROM user u WHERE 1') ;
    /*execution */
    $response->execute(array());

    return $response; /* reoutourne les information selectioner de tout nos users*/
}
/*fct qui affiche un utilisateur par l'id */
function getUser($userId)
{
    $bdd = dbConnect();// connection voir en bas 
    /*on prend tout dans la table user*/
    $response = $bdd->prepare ('SELECT * FROM user 
    WHERE id = :userId') ;
    /*on execute */
    $response->execute(array('userId' => $userId));
/* on retourne toute nos infos*/
    return $response; 
}

/* fct qui insert un nouvel utilisateur dans la bdd */
function createUser($bdd, $email, $password){
//    $bdd = dbConnect();
    /*on hash le mdp*/
    $encryptedPassword = password_hash($password, PASSWORD_DEFAULT);
    /*on prepare l'insertion de  l'email et le password dans la table user*/
    $response = $bdd->prepare ('INSERT INTO `blog`.user(`email`, `password`)
            VALUES (:email, :password)');
    /*on execute la preparation*/
    $response->execute(array('email' => $email, 'password' => $encryptedPassword));


    return $response; // on retourne la reponse

}
/* ceci est un gros copier coller en vue d'un update user*/
function updateArticle($articleId, $author_id, $title, $body){
    $bdd = dbConnect();

    $response = $bdd->prepare ('UPDATE `article` SET 
                                `author_id`= :author_id,
                                `title`= :title ,
                                `body`= :body
                                WHERE id = :articleId') ;

    $response->execute(array(   'articleId' => $articleId,
                                'author_id' => $author_id,
                                'body' => $body,
                                'title' => $title
    ));

    return $response;
}


/* ceci est un gros copier coller en vue d'un delete user*/
function deleteArticle($articleId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('DELETE FROM `article` WHERE id= :articleId') ;

    $response = $response->execute(array(   'articleId' => $articleId));

    return $response;
}


/* fct qui se connect a la bdd et retourne le pdo*/
function dbConnect(){
    try
    {
        return new PDO('mysql:host=localhost;dbname=blog;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}

