<?php
/*********voir le user en premier *****************/
/**************************************************/

$errors = new ArrayObject();// creation d'un tableau pour les erreurs
$articleId = getArticleIdFromURI(); 
// on recupere l'userid depuis la fct getUserIdFromURI 
$title="updateArticle " . $articleId;// titre pour template

require("../Model/articleRepository.php"); // require le model 

if(!$response = articleExist($articleId)){ // si article existe pas
    echo "This article does not exist"; // affiche erreur
    exit(); // tue le script
}

//var_dump($response);die;

if(isFormValid($errors)){ // si le form est valide on rentre dans le if

    /*on recupere nos variable*/
    $author = $_POST['author']; 
    $articleTitle = $_POST['title'];
    $body = $_POST['body'];

    $bdd = dbConnect(); // connect
    /* on appel la fct qui va update nos donnees*/
    $response = updateArticle($articleId, $author, $articleTitle, $body);

    $response->closeCursor(); /*on ferme le curseur*/

//    var_dump($_SERVER);
    header('Location: '.$_SERVER['REDIRECT_URL']); /*on redirige vers notre article si tous c'est bien passer*/
    die; //on tue le script
}

ob_start(); // voir user pour com
displayErrors($errors);
//var_dump($response);
$authorId = $response['author_id'];
$body = $response['body'];
$articleTitle = $response['title'];
require("../view/article/updateArticleView.php"); // voir user pour com

$content=ob_get_clean(); // voir user pour com

require("../view/templateView.php");// voir user pour com
 
/* pareil que pour le user mais diffenrte variable*/

// voir user pour com
function getArticleIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}
// voir user pour com
function articleExist($articleId){
    if($articleId == 0){
        return false;
    }
    return getArticle($articleId)->fetch();

}
// voir user pour com
function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }

    $author = $_POST['author'];
    $title = $_POST['title'];
    $body = $_POST['body'];
    if(!fieldsArefilled($author, $body, $title)){
        $errors->append('All the fields must be filled');
        return false;
    }

    return true;
}

// voir user pour com
function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['author']) AND isset($_POST['title']) AND isset($_POST['body'])){
        return true;
    }
    return false;
}

// voir user pour com
function fieldsArefilled($author, $title, $body){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($author) OR empty($body) OR empty($title)){
        return false;
    }
    return true;
}


function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}
