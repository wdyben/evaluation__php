<?php

function getArticles(){
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT a.*, u.email FROM article a
                                          LEFT JOIN user u
                                          ON a.author_id = u.id
                                          WHERE 1') ;

    $response->execute(array());

    return $response;
}

function getArticle($authorId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT a.*, u.email FROM article a
    LEFT JOIN user u
    ON a.author_id = u.id
    WHERE a.id = :id') ;

    $response->execute(array('id' => $authorId));

    return $response;
}


function createArticle($bdd, $authorId, $title, $body){
//    $bdd = dbConnect();

    $response = $bdd->prepare ('INSERT INTO `blog`.article(`author_id`, `title`, `body`)
            VALUES ( :authorId, :title, :body)');

    $response->execute(array('authorId' => $authorId,'title' => $title, 'body' => $body));

    return $response;

}
/* fct qui update l'article*/
function updateArticle($articleId, $author_id, $title, $body){
    $bdd = dbConnect();

    $response = $bdd->prepare ('UPDATE `article` SET 
                                `author_id`= :author_id,
                                `title`= :title ,
                                `body`= :body
                                WHERE id = :articleId') ;

    $response->execute(array(   'articleId' => $articleId,
                                'author_id' => $author_id,
                                'body' => $body,
                                'title' => $title
    ));

    return $response;
}


/* fct qui delete l'article*/
function deleteArticle($articleId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('DELETE FROM `article` WHERE id= :articleId') ;

    $response = $response->execute(array(   'articleId' => $articleId));

    return $response;
}


/*fct qui se co */
function dbConnect(){
    try
    {
        return new PDO('mysql:host=localhost;dbname=blog;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}

