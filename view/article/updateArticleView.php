<h1> <?= $title?> </h1>
<!-- vue du update qui  envoie nos variable au controlleur pour les verifier et les update -->
<form method="post" >

    <p> Qui est l'auteur de cet article ?  </p>
    <input type="text" name="author" value=<?= htmlspecialchars($authorId)?> />
    <p> Quel est le titre de votre article ?  </p>
    <input type="text" name="title" value=<?= htmlspecialchars($articleTitle)?> />
    <p> Que voulez vous dire dans votre article ? </p>
    <textarea name="body" ><?= htmlspecialchars($body)?></textarea>

    <input type="submit" value="valider">
</form>
